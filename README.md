# Chipflüstern

## Das Projekt

Wir möchten mit euch ein wenig in die Tiefen des Prozessors und seiner Peripherie tauchen. Du arbeitest auf einer leicht verfügbaren Plattform mit ESP32. Diese wirst du in Python programmieren und über I2C angebundene Peripheriegeräte ansprechen und steuern. Damit es kein Thermometer wird, gibt es einen 6DoF-Sensor MPU6050, der Lage und Beschleunigung messen kann. Dieser Sensor enthält erweiterte Funktionen, die du einzeln aktivieren und konfigurieren musst. Damit kannst du direkt im Sensor Tippen und Bewegung erkennen oder Schritte zählen. Dazu kommt ein ebenfalls über I2C angebundenes Display, und dein neues Produkt ist *fast* fertig.

## Die Aufgabe

### Die erste Aufgabe

**Zeichne eine Linie auf dem Display, die immer parallel zum Fußboden ist.**

Das klingt einfach, ist aber eine _komplexe Aufgabe_. Diese muss man als Anwendungsentwickler solange _auf kleinere Einheiten herunterbrechen_, bis man die so entstandenen einfachen Teilaufgaben als Computerprogramm implementieren kann.

* Erstes Herunterbrechen - Vereinfachen:

    * Zu Beginn werden nur die zwei Dimensionen verwendet, die in der Ebene mit dem Display sind.
    * Da drei Dimensionen möglich sind, sollten diese bereits jetzt im Entwurf vorgesehen werden, damit die Erweiterung auf drei Dimensionen einfach erfolgen kann.


* Zweites Herunterbrechen - Aufteilen:

    * Lesen des Sensors

    Diese Gruppe liest die Werte aus dem Sensor und bereitet sie so auf, dass sie in Echtzeit auf dem 128x64 Pixel großen Display angezeigt werden können.

    * Darstellung auf dem Display

    Diese Gruppe erwartet als Eingabe zwei Koordinatenpaare (x1,y1) und (x2,y2) und zeichnet zwischen diesen eine Linie. Idealerweise läuft die Anzeigeroutine so schnell, dass die Linie flacker- und ruckelfrei der Bewegung des Sensors folgt.

    Diese Unterteilung der Aufgabe erfordert eine klar definierte Schnittstelle zwischen Sensor und Display.

### Die zweite Aufgabe

**Zeichne ein 3D-Objekt ("Teekanne") auf das Display, das der Bewegung des Sensors in Echtzeit flackerfrei folgt.**

### Die dritte Aufgabe

Die dritte Aufgabe **ergibt sich** im Verlauf des Projekts. Klassisch wäre die Verwendung in einem UAV, Unmanned Aerial Vehicle.

## Das verwendete Gerät

### Ein PC als Programmierumgebung

Da der ESP32 nicht über eine 'normale' Benutzerschnittstelle mit Tastatur, Maus und großem Bildschirm verfügt, braucht man einen 'normalen' PC, Laptop oder Raspberry Pi. Dieser stellt zum einen die Mensch-Maschine-Schnittstelle zur Verfügung, so dass du Code schreiben kannst. Zum anderen sorgt er für die Verbindung zur ESP32-Platine, so dass du dort interaktiv Python ausprobieren und dein Programm starten kannst.

* Die vorinstallierten Dateien erstmalig aus ihrem Git-Repository holen

    ```
    git clone https://gitlab.com/ausbildungsleiter77/chipfluestern.git
    ```

* Die ursprünglichen Dateien aus dem Git-Repostory wiederherstellen

    Jeder kann sich einmal vertun oder verirren. Dann ist es gut, wenn man den Startzustand wiederherstellen kann. Zuerst muss man die 'kaputten' Dateien löschen, dann kann man sie mit einem einzigen Kommando wiederherstellen.

    ```
    git pull
    ```

    Git kann viel mehr, aber eine Einführung in Git würde hier den Rahmen sprengen. Selbstverständlich darfst du auftretende merge-Konflikte auch lösen...

* Python-Code in einem Editor erstellen

    Du kannst auf dem PC mit dem Editor des Vertrauens (Notepad++, Geany, VS Code, ...) Python-Programme erstellen, diese speichern und in einem separaten Schritt auf den ESP32 übertragen.

* Python-Code auf den ESP32 übertragen

    Da MicroPython bisher nicht als Laufwerk ähnlich wie ein USB-Stick auf dem PC erscheint, muss man Dateien in einem separaten Schritt hochladen. Dazu gibt es das Programm  ```ampy```. Dieses Programm ist ein Kommandozeilen-Programm _ohne_ grafische Oberfläche. ```ampy``` läuft auf dem PC, in einem Kommandofenster auf dem PC. Es kommuniziert mit dem ESP32 und kann Dateien von und zum ESP32 kopieren.

    ```ampy``` kann nur über eine _freie Leitung_ mit dem ESP32 kommunizieren. Man muss also vor der Nutzung von ```ampy``` die serielle Verbindung mit minicom oder TeraTerm beenden.

    ```
    ampy -p COM14 -b 115200 -d 1 reset
    ampy -p COM14 -b 115200 ls
    ampy -p COM14 -b 115200 put meine_datei.py
    ```

### ESP32 mit MicroPython

Der ESP32 ist ein eigener Computer. Auf dem ESP32 ist MicroPython 1.12 installiert. Er hat allerdings keine Tastatur und nur einen kleinen Bildschirm. Man verbindet sich für die Programmentwicklung mit dem ESP32 über eine serielle Verbindung. Diese erfolgt über einen USB-Seriell-Wandler, so dass die Platine an einem USB-Anbschluss des PCs mit demselben verbunden ist.

Um sich mit dem ESP32 zu verbinden, nutzt man ein Terminalprogramm. Das Terminalprogramm läuft auf dem PC. Es _sieht aus_ wie ein Konsolenfenster. Es ist aber _nicht_ auf dem PC, sondern zeigt ein Befehlsfenster auf einem _separaten Computer_, dem ESP32.

Unter _Windows_ ist **TeraTerm** die erste Wahl, da es alle seriellen Schnittstellen erkennt und anzeigt. Unter _Linux_ kann man **minicom** verwenden.

Die serielle Verbindung ist exclusiv, sie kann nur von einem Programm genutzt werden. Man kann also _entweder_ im Terminal verbunden sein _oder_ mit ```ampy``` Dateien hochladen.

Es sind bereits die notwendigen Dateien für die Kommunikation mit dem Sensor und dem Display installiert. Außerdem ein Testprogramm und ein Ansatzpunkt für eine eigene Grafikbibliothek.

* Verbindung aufnehmen

    Man spricht das Board über die serielle Schnittstelle an. Das geht beispielsweise mit Minicom,Putty, TeraTerm, ...  Unter Linux, also auch auf dem Raspberry Pi, ist die serielle Schnittstelle /dev/ttyUSB0 zu verwenden. Unter Windows ist es COMx, die jeweilige Nummer _x_ findet man im Gerätemanager. Das Programm TeraTerm erkennt selber alle angeschlossenen seriellen Ports. Die Geschwindigkeit beträgt 115200 Baud.

    ```
    minicom -b 115200 -D /dev/ttyUSB0 -c on
    ```

    Wenn man mit dem Board verbunden ist, kann man dort direkt interaktiv in Python programmieren.

* Erste Schritte

    Am Anfang steht sicher ein interaktives Erproben der mitgelieferten Module im REPL. REPL steht für "Read Eval Print Loop" und ist eine gängige Bezeichnung für das interaktive Prompt einer Programmiersprache. Um die REPL zu nutzen, muss man sich mit einem Terminalprogramm mit dem ESP32 verbinden.

* Wiederherstellen des Startzustands auf dem ESP32

    Auch auf dem ESP32 kann Durcheinander entstehen. In diesem Fall kann man die entsprechenden Dateien vom PC mit Hilfe von ```ampy``` wieder auf den ESP32 kopieren. ```ampy``` überschreibt die dort vorhandenen Dateien _ohne Rückfrage_.

    ```
    ampy -p COM14 -b 115200 -d 1 reset
    ampy -p COM14 -b 115200 ls
    ampy -p COM14 -b 115200 put bib/ssd1306/ssd1306.py
    ampy -p COM14 -b 115200 put bib/mpu6050/cfilter.py
    ampy -p COM14 -b 115200 put bib/mpu6050/constants.py
    ampy -p COM14 -b 115200 put bib/mpu6050/mpu6050.py
    ampy -p COM14 -b 115200 put example/grafik.py
    ampy -p COM14 -b 115200 put example/testprogramm.py
    ampy -p COM14 -b 115200 ls
    ```

### Die Bausteine

* I<sup>2</sup>C-Bus

    Der I<sup>2</sup>C-Bus, kurz für Inter Integrated Circuit-Bus wurde 1982 bei Philips entwickelt. Er wird hauptsächlich für die Kommunikation zwischen Microcontroller und Peripherie-ICs verwendet. So auch in diesem Projekt.

    ```
    help()
    import machine
    i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(21))
    i2c.scan()
    [60, 104]
    ```

    60 == 0x3C -- Adresse des SSD1306 OLED-Displays

    104 == 0x68 -- Adresse des MPU6050 Sensors


* Display

    Das Display ist über die Funktionen in der Datei ssd1306.py ansprechbar.

    ```
    import machine
    import ssd1306
    i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(21))
    oled = ssd1306.SSD1306_I2C(128,64, i2c)
    oled.fill(0)
    oled.text('MicroPython', 10, 10)  # 'Text', Spalte, Zeile
    oled.text('on ESP32', 10, 30)
    oled.text("............", 10, 50)
    oled.show()
    ```

* Sensor

    Der  Sensor ist über die Funktionen in der Datei mpu605.py ansprechbar.

    ```
    import mpu6050
    import machine
    sensor = mpu6050.MPU(scl=22,sda=21)
    * initializing pins
    * initializing i2c
    * initializing mpu
    * identifying i2c device
    sensor.read_sensors()
    [17950, 98, 1910, -3358, -476, 57, 129]
    ```

## DMP

DMP steht für Digital Motion Processor und ist eine im Sensor enthaltenen Einheit. Mit Hilfe des DMP kann man die Daten von beiden Sensorteilen fusionieren und auch fortgeschrittene Funktionen wie Tap-Detection direkt im Sensor erledigen lassen. Um diese Funktionen nutzen zu können, muss man sich länger mit dem Sensor und den verfügbaren Informationen beschäftigen. Der DMP ist lediglich durch Code-Beispiele und Reverse-Engineering dokumentiert. Man benötigt außerdem Teile einer proprietären, undokumentierten Firmware.

### Quellen

* Beim Hersteller Invensense

    Nach Registrierung bekommt man Zugang zur Entwicklerdoku bei invensense.com.

    ```
    https://www.invensense.com/developers/login/
    ```

    Dort findet sich keine direkte Dokumentation. Man darf sich also alle Informationen aus den mitgeliefertene Headern und Beispielen zusammen suchen.

* Andere Quellen

    ```
    https://www.i2cdevlib.com/devices/mpu6050#source
    http://www.geekmomprojects.com/mpu-6050-redux-dmp-data-fusion-vs-complementary-filter/
    http://www.geekmomprojects.com/mpu-6050-dmp-data-from-i2cdevlib/
    ```

    Therefore the next step was to figure out how to extract data from the DMP. Fortunately, Jeff Rowberg has written and made public a very useful library, i2cdevlib, which does just that.  It is available for download at:
    ```
    https://github.com/jrowberg/i2cdevlib
    https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/MPU6050
    ```

    To use it with the MPU 6050, you will need the library functions in the folder labeled Arduino/MPU6050.  To install the library, simply copy the Arduino/MPU6050 folder to the same location as your other Arduino libraries.

* DMP _einfach_ verwenden

    ```
    https://www.invensense.com/developers/forums/topic/mpu-60x0-dmp/
    https://github.com/bzerk/MPU6050_DMP_6_axis_demo_/blob/master/MPU6050_DMP_6_axis_demo_.pde
    https://www.i2cdevlib.com/tools/analyzer/1
    https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/MPU6050/examples/MPU6050_DMP6
    https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/MPU6050_6Axis_MotionApps_V6_12.h
    ```

* Weitere Informationen

    Could you publish the COM data packet format so that I can correctly parse it, or provide some pointers on to how to get MotionApps 1.1.1 to function correctly with the 60x0.

    July 7, 2011 at 7:29 pm #17313 REPLY -- Here is the COM packet description
    ```
    packet:
    Content $ 03h x gyro H x gyro L y gyro H y gyro L z gyro H z gyro L byte 1 2 3 4 5 6 7 8

    x accel H x accel L y accel H y accel L z accel H z accel L 9 10 11 12 13 14

    x mag H x mag L y mag H y mag L z mag H z mag L button 1 LineFeed 15 16 17 18 19 20 21 22 23
    ```
