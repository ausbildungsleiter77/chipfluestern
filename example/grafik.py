#
'''
Eine eigene Grafikbibliothek
Copyright 2020 Roland Härter

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

'''
    Die Bibliothek ssd1306.py enthält nur die allernötigsten Funktionen:
    * fill()
    * pixel()
    * scroll()
    * text()

Für die Darstellung einer Linie auf dem Display, die der Bewegung des
Geräts und somit den Messwerten des Senosrs folgt, braucht es eigene,
selbsterstellte Funktionalität.
'''

# eine erst einmal leere eigene Funktion linie()
def linie (oled, x1, y1, x2, y2):
    '''
      Gerade Linien sind einfach. Man muss z.B. bei gleichem y für alle
      Punkte von x_start,y bis x_ende,y die Funktion pixel() aufrufen.
      Was aber wenn x1 != x2 _und_ y1 != y2?
      Wie kann man das auf einem 128x64 Pixel großen Display abbilden?
      Genau das braucht es für eine Linie, die der Bewegung des
      Boards (idealerweise in Echtzeit) folgt.
    '''
    pass

# ein Display erstellen, ohne das geht nichts
def init_display():
    import machine
    import ssd1306
    i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(21))
    oled = ssd1306.SSD1306_I2C(128,64, i2c)
    return oled

# einen Startbildschirm anzeigen
def startbild(oled):
    oled.fill(0)
    oled.text('MicroPython', 10, 10)  # 'Text', Spalte, Zeile
    oled.text('on ESP32', 10, 20)
    oled.text("............", 10, 25)
    oled.show()

# das Hauptprogramm erstellt ein Display und ruft startbildschirm() auf
def hauptprogramm():
    oled = init_display()
    startbild(oled)

# eine Abkürzung für einfacheres Schreiben
def go():
    hauptprogramm()

# _kein_ automatischer Start
if __name__ == "__main__":
    pass
