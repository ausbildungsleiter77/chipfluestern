#
'''
Testprogramm
Copyright 2020 Roland Härter

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

def hauptprogramm():
    import machine
    import mpu6050
    import ssd1306
    i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(21))
    sensor = mpu6050.MPU(scl=22,sda=21)

    result = sensor.read_sensors()
    accel = ''.join((str(result[0])," ",str(result[1])," ",str(result[2])))
    temp  = str(result[3])
    gyro  = ''.join((str(result[4])," ",str(result[5])," ",str(result[6])))
    oled = ssd1306.SSD1306_I2C(128,64, i2c)

    oled.fill(0)
    oled.text('MicroPython', 10, 10)  # 'Text', Spalte, Zeile
    oled.text('on ESP32', 10, 20)
    oled.text("............", 10, 25)
    oled.text(accel, 0, 35)
    oled.text(temp, 10, 45)
    oled.text(gyro,  0, 55)

    oled.show()

def go():
    hauptprogramm()

if __name__ == "__main__":
    pass
